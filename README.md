# TrivialGame

Developers: Marc Toboso (mtoboso) and Cristina Martin (crisxiaoyue)

Trivial Game made with Vue Components and Laravel API and Voyager for all the CRUD operations

Vue libraries used on the project:
*  vue-router
*  Toasted
*  vuex (storage)


Screenshots:

![Screenshot](Imagen 1.png)

![Screenshot](Imagen 2.png)

![Screenshot](Imagen 4.png)

Administration Voyager:

![Screenshot](Imagen 5.png)

![Screenshot](Imagen 6.png)