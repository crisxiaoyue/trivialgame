<?php

use Illuminate\Database\Seeder;
use App\Resposta;

class RespostesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('respuestas')->delete();

        foreach( $this->arrayRespostes as $resposta ) {
            $myReposta = new Resposta;
            $myReposta->idPregunta = $resposta['idPregunta'];
            $myReposta->resposta = $resposta['resposta'];
            $myReposta->isCorrecta = $resposta['isCorrecta'];
            $myReposta->save();
        }
    }

    private $arrayRespostes = array(
        array(
            'idPregunta' => '1',
            'resposta' => 'Islamabad',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '1',
            'resposta' => 'Karachi',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '1',
            'resposta' => 'Lahore',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '1',
            'resposta' => 'Sukkur',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '2',
            'resposta' => 'Perú',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '2',
            'resposta' => 'Venezuela',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '2',
            'resposta' => 'Argentina',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '2',
            'resposta' => 'Mèxic',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '3',
            'resposta' => 'Per Venezuela',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '3',
            'resposta' => 'Per Argentina',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '3',
            'resposta' => 'Per Perú',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '3',
            'resposta' => 'Per Mèxic',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '4',
            'resposta' => 'EE.UU.',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '4',
            'resposta' => 'Portugal',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '4',
            'resposta' => 'Espanya',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '4',
            'resposta' => 'França',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '5',
            'resposta' => 'Àfrica',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '5',
            'resposta' => 'Europa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '5',
            'resposta' => 'Àsia',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '5',
            'resposta' => 'Amèrica',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '6',
            'resposta' => 'A Àfrica',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '6',
            'resposta' => 'A Oceania',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '6',
            'resposta' => 'A Àsia',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '6',
            'resposta' => 'A Amèrica',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '7',
            'resposta' => 'Refah',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '7',
            'resposta' => 'Cachemira',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '7',
            'resposta' => 'Amán',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '7',
            'resposta' => 'Taskent',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '8',
            'resposta' => 'Oregón',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '8',
            'resposta' => 'Florida',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '8',
            'resposta' => 'Massachusetts',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '8',
            'resposta' => 'Nova York',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '9',
            'resposta' => 'San Salvador',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '9',
            'resposta' => 'L\'habana',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '9',
            'resposta' => 'Tecucigalpa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '9',
            'resposta' => 'Caraques',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '10',
            'resposta' => 'Conill',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '10',
            'resposta' => 'Jirafa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '10',
            'resposta' => 'Koala',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '10',
            'resposta' => 'Okapi',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '11',
            'resposta' => 'Estocolm',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '11',
            'resposta' => 'Moscú',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '11',
            'resposta' => 'Berlín',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '11',
            'resposta' => 'Praga',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '12',
            'resposta' => 'Oscar Niemeyer',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '12',
            'resposta' => 'Ioeh Ming Pei',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '12',
            'resposta' => 'Luis Barragán Morgín',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '12',
            'resposta' => 'Philip Johnson',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '13',
            'resposta' => 'Michael Crichton',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '13',
            'resposta' => 'John Grisham',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '13',
            'resposta' => 'Umberto Eco',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '13',
            'resposta' => 'Leopoldo Alas Clarín',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '14',
            'resposta' => 'A la novel·la cortesana',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '14',
            'resposta' => 'A l\'ensaig',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '14',
            'resposta' => 'A la novel·la picaresca',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '14',
            'resposta' => 'A la novel·la de caballeria',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '15',
            'resposta' => 'Neogótic',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '15',
            'resposta' => 'Costumbrisme',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '15',
            'resposta' => 'Símbolisme',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '15',
            'resposta' => 'Cubisme',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '16',
            'resposta' => 'Premi Nobel',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '16',
            'resposta' => 'Permi Planeta',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '16',
            'resposta' => 'Premi Princep d\'Asturies de les Lletres',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '16',
            'resposta' => 'Premi Cervantes',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '17',
            'resposta' => 'Està mort',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '17',
            'resposta' => 'S\'incendia la granja en la que treballa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '17',
            'resposta' => 'Està malalt',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '17',
            'resposta' => 'Fa un viatge astral',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '18',
            'resposta' => 'Antonio Moro',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '18',
            'resposta' => 'Pieter Brueghel el Joven',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '18',
            'resposta' => 'Bronzino',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '18',
            'resposta' => 'Alberto Durero',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '19',
            'resposta' => 'Pierre-Augustin de Beaumarchais',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '19',
            'resposta' => 'Cesare Sterbini',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '19',
            'resposta' => 'Prosper Mérimée',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '19',
            'resposta' => 'Lorenzo da Ponte',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '20',
            'resposta' => 'e',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '20',
            'resposta' => 'Q',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '20',
            'resposta' => 'Z',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '20',
            'resposta' => 'f',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '21',
            'resposta' => 'Espanya',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '21',
            'resposta' => 'Anglaterra',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '21',
            'resposta' => 'Portugal',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '21',
            'resposta' => 'Holanda',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '22',
            'resposta' => 'Hernán Cortes',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '22',
            'resposta' => 'Vasco Núñez de Balboa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '22',
            'resposta' => 'Juan Ponce de León',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '22',
            'resposta' => 'Francisco Pizarro',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '23',
            'resposta' => 'Un sacerdot de Chichén Itzá',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '23',
            'resposta' => 'Un campió de lluita lliure',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '23',
            'resposta' => 'Un cacic mixteco',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '23',
            'resposta' => 'El Déu de la guerra olmeca',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '24',
            'resposta' => 'Del segle XVIII a XX',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '24',
            'resposta' => 'Del segle V al XV',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '24',
            'resposta' => 'Del segle XV al XVIII',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '24',
            'resposta' => 'Del segle I al V',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '25',
            'resposta' => 'Parel·lel 38',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '25',
            'resposta' => 'Paral·lel 177',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '25',
            'resposta' => 'Paral·lel 29',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '25',
            'resposta' => 'Paral·lel 5',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '26',
            'resposta' => 'José Maria Aznar Lopez',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '26',
            'resposta' => 'Adolfo Suárez Gonzalez',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '26',
            'resposta' => 'Felipe González Márquez',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '26',
            'resposta' => 'Leopoldo Calvo-Sotelo',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '27',
            'resposta' => 'Alemània',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '27',
            'resposta' => 'Holanda',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '27',
            'resposta' => 'Espanya',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '27',
            'resposta' => 'EE.UU.',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '28',
            'resposta' => 'El Ferrol (La Corunya)',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '28',
            'resposta' => 'Madrid',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '28',
            'resposta' => 'El Escorial (Madrid)',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '28',
            'resposta' => 'Ponferrada (Lleó)',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '29',
            'resposta' => '1845',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '29',
            'resposta' => '1929',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '29',
            'resposta' => '1829',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '29',
            'resposta' => '1945',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '30',
            'resposta' => 'Timoteo Vandor',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '30',
            'resposta' => 'General Perón',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '30',
            'resposta' => 'General Aramburu',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '30',
            'resposta' => 'General Onganía',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '31',
            'resposta' => 'Dani Flein',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '31',
            'resposta' => 'Amaia Montero',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '31',
            'resposta' => 'Joey Tempest',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '31',
            'resposta' => 'Soledad Giménez',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '32',
            'resposta' => 'Argentina',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '32',
            'resposta' => 'Xile',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '32',
            'resposta' => 'Canadà',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '32',
            'resposta' => 'Russia',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '33',
            'resposta' => 'José Luis Perales',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '33',
            'resposta' => 'Víctor Manuel',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '33',
            'resposta' => 'Carlos Baute',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '33',
            'resposta' => 'Luis Eduardo Aute',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '34',
            'resposta' => 'Cifras y letras',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '34',
            'resposta' => 'Password',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '34',
            'resposta' => 'Saber y ganar',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '34',
            'resposta' => 'Pasapalabra',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '35',
            'resposta' => 'Boca a boca',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '35',
            'resposta' => 'Entre les cames',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '35',
            'resposta' => 'Coses que fan que la vida valgui la pena',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '35',
            'resposta' => 'Reines',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '36',
            'resposta' => 'Japó',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '36',
            'resposta' => 'Sibèria',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '36',
            'resposta' => 'Índia',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '36',
            'resposta' => 'Xina',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '37',
            'resposta' => 'Espanya',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '37',
            'resposta' => 'Itàlia',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '37',
            'resposta' => 'França',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '37',
            'resposta' => 'Suíssa',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '38',
            'resposta' => 'Jean Renoir',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '38',
            'resposta' => 'Marguerite Duras',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '38',
            'resposta' => 'Krysztof Kieslowski',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '38',
            'resposta' => 'Françoise Truffaut',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '39',
            'resposta' => 'Portugal',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '39',
            'resposta' => 'Brasil',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '39',
            'resposta' => 'França',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '39',
            'resposta' => 'Espanya',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '40',
            'resposta' => 'Rol',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '40',
            'resposta' => 'Plataformes',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '40',
            'resposta' => 'Aventura gràfica',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '40',
            'resposta' => 'Arcade',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '41',
            'resposta' => 'Equinocci de setembre',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '41',
            'resposta' => 'Equinocci de març',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '41',
            'resposta' => 'Solstici de setembre',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '41',
            'resposta' => 'Solstici de març',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '42',
            'resposta' => 'Obsesió pels objectes inservibles',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '42',
            'resposta' => 'Complexe d\'inferioritat',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '42',
            'resposta' => 'Obsesió per la neteja',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '42',
            'resposta' => 'Síndrome de l\'home que mai crèix',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '43',
            'resposta' => 'Rotació',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '43',
            'resposta' => 'Libració',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '43',
            'resposta' => 'Selenisme',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '43',
            'resposta' => 'Traslació',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '44',
            'resposta' => 'Màscle',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '44',
            'resposta' => 'Hermafrodíta',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '44',
            'resposta' => 'Femella',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '44',
            'resposta' => 'Mai passa en el ser humà',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '45',
            'resposta' => 'Rebuig per la gent gran',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '45',
            'resposta' => 'Terror als espais públics',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '45',
            'resposta' => 'Por a les papallones',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '45',
            'resposta' => 'Terror als animals',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '46',
            'resposta' => 'Freud',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '46',
            'resposta' => 'Wundt',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '46',
            'resposta' => 'Skinner',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '46',
            'resposta' => 'Pávlov',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '47',
            'resposta' => 'Alva Edison',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '47',
            'resposta' => 'Samuel Colt',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '47',
            'resposta' => 'Gutenbert',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '47',
            'resposta' => 'Guglielmo Marconi',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '48',
            'resposta' => 'S\'uneix a l\'estern mitjançant un os',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '48',
            'resposta' => 'No s\'uneix al estern',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '48',
            'resposta' => 'S\'uneix al estern a través del cartíleg d\'una altra costella.',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '48',
            'resposta' => 'S\'uneix al estern a través del seu pròpi cartíleg',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '49',
            'resposta' => 'Calendari xino',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '49',
            'resposta' => 'Va ser el primer calendari de la història',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '49',
            'resposta' => 'Calendari julià',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '49',
            'resposta' => 'Calendari perpetu',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '50',
            'resposta' => 'La radiació cosmica',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '50',
            'resposta' => 'Els rajos X',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '50',
            'resposta' => 'El neutró',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '50',
            'resposta' => 'L\'Aragò',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '51',
            'resposta' => 'Dallas',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '51',
            'resposta' => 'Los Ángeles',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '51',
            'resposta' => 'Milwaukee',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '51',
            'resposta' => 'San Antonio',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '52',
            'resposta' => 'Argentina',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '52',
            'resposta' => 'Suíssa',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '52',
            'resposta' => 'Russa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '52',
            'resposta' => 'Espanyola',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '53',
            'resposta' => 'El número de cops que un jugador necessita per arribar al green',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '53',
            'resposta' => 'Els forats de sorra que hi ha al camp',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '53',
            'resposta' => 'El moviment oscil·lant que es fa al golpejar la bola',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '53',
            'resposta' => 'L\'últim cop, amb el que entra la pilota al forat',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '54',
            'resposta' => 'Fútbol',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '54',
            'resposta' => 'Formula 1',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '54',
            'resposta' => 'Cricket',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '54',
            'resposta' => 'Golf',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '55',
            'resposta' => 'Everest',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '55',
            'resposta' => 'Anapurna',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '55',
            'resposta' => 'K-2',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '55',
            'resposta' => 'Nanga Parvat',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '56',
            'resposta' => 'Romario',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '56',
            'resposta' => 'Iván Zamorano',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '56',
            'resposta' => 'Dani Guiza',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '56',
            'resposta' => 'Enrique Castro \"Quini\"',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '57',
            'resposta' => '15x30',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '57',
            'resposta' => '20x10',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '57',
            'resposta' => '25x50',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '57',
            'resposta' => '28x14',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '58',
            'resposta' => 'Una medalla',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '58',
            'resposta' => 'Una copa',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '58',
            'resposta' => 'Un anell',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '58',
            'resposta' => 'Un diploma',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '59',
            'resposta' => 'Alberto Contador',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '59',
            'resposta' => 'Cris Froome',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '59',
            'resposta' => 'Juanjo Cobo',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '59',
            'resposta' => 'Andy Schleck',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '60',
            'resposta' => 'Jack Nicklaus',
            'isCorrecta' => true
        ),
        array(
            'idPregunta' => '60',
            'resposta' => 'Severuabi Ballesteros',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '60',
            'resposta' => 'Tiger Woods',
            'isCorrecta' => false
        ),
        array(
            'idPregunta' => '60',
            'resposta' => 'Sergiro García',
            'isCorrecta' => false
        )
    );
}
