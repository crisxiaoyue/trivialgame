<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->delete();

        foreach( $this->arrayCategories as $categoria ) {
            $myCategoria = new Categoria;
            $myCategoria->nom = $categoria['nom'];
            $myCategoria->descripcio = $categoria['descripcio'];
            $myCategoria->save();
        }
    }

    private $arrayCategories = array(
        array(
            'nom' => 'Geografía',
            'descripcio' => 'Blau'
        ),
        array(
            'nom' => 'Art i literatura',
            'descripcio' => 'Marró'
        ),
        array(
            'nom' => 'Història',
            'descripcio' => 'Groc'
        ),
        array(
            'nom' => 'Entreteniment',
            'descripcio' => 'Rosa'
        ),
        array(
            'nom' => 'Ciències i natura',
            'descripcio' => 'Verd'
        ),
        array(
            'nom' => 'Esports i passatemps',
            'descripcio' => 'Taronja'
        )
    );
}
