<?php

use Illuminate\Database\Seeder;
use App\Pregunta;

class PreguntesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('preguntas')->delete();

        foreach( $this->arrayPreguntes as $pregunta ) {
            $myPregunta = new Pregunta;
            $myPregunta->idCategoria = $pregunta['idCategoria'];
            $myPregunta->pregunta = $pregunta['pregunta'];
            $myPregunta->pista = $pregunta['pista'];
            $myPregunta->save();
        }
    }

    private $arrayPreguntes = array(
        array(
            'idCategoria' => '1',
            'pregunta' => 'Quina és la capital de Pakistan?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'A quin país hi ha el llac Maracaibo?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'Per quin país passa el riu Orinoco?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'A quin país pertanyen les Illes Azores?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'A quin continent hi ha el riu Amur?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'A quin continent hi ha el riu Misuri?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'Quina és la capital de Uzbekistan?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'Quin és l\'estat d\'Estats Units que està més al oest?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'Quina és la capital de la República de El Salvador?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '1',
            'pregunta' => 'A més del cangur, quin és l\'animal és el símbol d\'Austàlia?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'A quina ciutat es troba l\'edifici desconstructivista: La Casa Danzante?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Qui va ser l\'arquitecte de la Catedral de Brasilia?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Qui va escriure El pendol de Foucault?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'A quin genere pertany El Lazarillo de Tormes?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'A quin moviment artistic pertany El beso de Klimt?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Quin és el premi de la literatura més antic d\'Espanya?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Que li passa a en Mario a l\'obra de Miguel Delibres \'Cinco horas con Mario\'?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Qui va fer el famos retrat de la reina María Tudor?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Qui és l\'autor de Carmen, novel·la publicada el 1845?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '2',
            'pregunta' => 'Quina butaca de la RAE va ocupar Camilo José Cela?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Quin país va conquistar les Filipines al segle XVI?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Qui va ser l\'explorador i conquistador español del Perú?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Qui va ser Ocho Venado Garra de Jaguar?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Quins segles comprenen el període històric conegut com Edat Mitja??',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Per quin paral·lel va ser dividia Corea en dos parts desrpès de la II Guerra Mondial?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Qui va ser el primer president d\'Espanya desprès de la dictadura d\'en Franco?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Quin país va ser el primer del mon en permetre l\'adopció a parelles del mateix sexe?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'On va neixer Francisco Franco?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Quin any va ser el dijous negre a la Borsa de Nova York que va donar l\'inici a La Gran Depresió?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '3',
            'pregunta' => 'Qui va derrocar al president argentí Arturo Illia?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'Com es diu la cantant del grup belga Vaya con Dios?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'De quin país es la raça de gos Samoyedo?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'Qui va compondre les cançons del disc \'Marinero de luces\' per la Isabel Pantoja?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'Com es diu el concurs en que els participants han d\'endivinar paraules amb les pistes que els hi donen els artistes convidats?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'Quina pel·lícula de Manuel Gomez Pereira narra la vida d\'una adicta al sexe, el seu marit i el seu aman?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'De quin pais és la raça de gos Husky?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'D\'on és el formatge elemental?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'Quina és el director de la triología Tres colors: Vermell - Blau i Blanc?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'D\'on és típic el Bacallà?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '4',
            'pregunta' => 'De quin tipus es el videojoc anomenat Arkanoid?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Al hemisferi nord, quin moment marca el pas del estiu a la tardor?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Quin és el complexe de Peter Pan?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Com es diu el moviment pel que la lluna gira sobre ella mateixa?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Quin sexe serà una convinació dels comosomes XY?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Que és la Gerontofobia?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Quin psicoleg va ser el primer en crear un laboratori de psicologia, convertint aquesta disciplina a ciència?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Qui va inventar la impremta??',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Que caracteritza a les costelles falses?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Quin calendari s\'utilitzava a Europa abans del actual calendari georgià?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '5',
            'pregunta' => 'Que va descrobrir James Chadwick?',
            'pista' => ''
        ),array(
            'idCategoria' => '6',
            'pregunta' => 'En quina ciutat d\'EEUU tenen la seu Els Mavericks, l\'equip de la NBA?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'De quina nacionalitat és en Roger Federer?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'En el golf, que és el Swing?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'A que juga David Beckham?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'Quins 8.000 es van pujar primer?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'Qui va ser el pichichi de la Lliga española de fútbol tres temporades seguides 1979-82?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'Quan madeix una pista de padel?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'Quin trofeu reben els membres de l\'equip guanyador de la lliga regular de la NBA?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'Qui va guanyar la Volta a Espanya el 2011?',
            'pista' => ''
        ),
        array(
            'idCategoria' => '6',
            'pregunta' => 'Qui és el golfista conciderat el millor de la història per haver guanyat més tornejos majors...',
            'pista' => ''
        )
    );
}