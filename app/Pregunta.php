<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $table = 'preguntas';
    protected $fillable = ['idCategoria', 'pregunta', 'pista'];

    public function respostes(){
        return $this->hasMany('App\Resposta', 'idPregunta', 'id');
    }
}
