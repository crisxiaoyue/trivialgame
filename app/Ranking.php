<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ranking extends Model
{
    protected $table = 'Ranking';
    protected $fillable = ['user', 'partides', 'temps'];

}