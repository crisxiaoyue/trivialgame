<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resposta extends Model
{
    //
    protected $table = 'respuestas';
    protected $fillable = ['idPregunta', 'resposta', 'isCorrecta'];

}
