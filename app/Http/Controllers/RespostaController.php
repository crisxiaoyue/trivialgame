<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resposta;

class RespostaController extends Controller
{
    public function all(){
        return response()->json(Resposta::all());
    }

    public function id($id){
        return response()->json(Resposta::findOrFail($id));
    }

    public function update($id, Request $request){
        $_resposta = Resposta::findOrFail($id);

        $input = $request->all();
        $_resposta->fill($input)->save();

        return response()->json($_resposta);
    }

    public function add(Request $request){
        $_resposta = new Resposta();

        $input = $request->all();
        $_resposta->fill($input)->save();

        return response()->json($_resposta);
    }

    public function delete($id){
        $_resposta = Resposta::findOrFail($id);
        $_resposta->delete();

        return "OK";
    }

    //Retorna si la resposta és correcta o falsa
    public function isCorrect($id){
        $_resposta = Resposta::findOrFail($id);

        if($_resposta->isCorrecta){
            return 'true';
        } else{
            return 'false';
        }
    }


}
