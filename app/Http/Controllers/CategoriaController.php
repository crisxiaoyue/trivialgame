<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    public function all(){
        return response()->json(Categoria::all());
    }

    public function id($id){
        return response()->json(Categoria::findOrFail($id));
    }

    public function update($id, Request $request){
        $_categoria = Categoria::findOrFail($id);

        $input = $request->all();
        $_categoria->fill($input)->save();

        return response()->json($_categoria);
    }

    public function add(Request $request){
        $_categoria = new Categoria();

        $input = $request->all();
        $_categoria->fill($input)->save();

        return response()->json($_categoria);
    }

    public function delete($id){
        $_categoria = Categoria::findOrFail($id);
        $_categoria->delete();

        return "OK";
    }
}
