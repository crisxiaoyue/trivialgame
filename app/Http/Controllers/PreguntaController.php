<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pregunta;
use App\Resposta;
use Illuminate\Support\Facades\DB;

class PreguntaController extends Controller
{
    public function all(){
        return response()->json(Pregunta::all());
    }

    public function id($id){
        return response()->json(Pregunta::findOrFail($id));
    }

    public function update($id, Request $request){
        $_pregunta = Pregunta::findOrFail($id);

        $input = $request->all();
        $_pregunta->fill($input)->save();

        return response()->json($_pregunta);
    }

    public function add(Request $request){
        $_pregunta = new Pregunta();

        $input = $request->all();
        $_pregunta->fill($input)->save();

        return response()->json($_pregunta);
    }

    public function delete($id){
        $_pregunta = Pregunta::findOrFail($id);
        $_pregunta->delete();

        return "OK";
    }

    //Retorna una pregunta aleatoria de la categoria sol·licitada.
    public function categoria($categoria){
        //Retorna totes les preguntes d'una categoria concreta.
        $preguntes_categoria = Pregunta::where('idCategoria', '=', $categoria)->get();

        //Conta quantes preguntes d'una categoria hi ha.
        $numPreguntes = count($preguntes_categoria);

        //S'agafara la pregunta que estigui a la posició següent
        $posicio = rand(0, $numPreguntes-1);

        return response()->json($preguntes_categoria[$posicio]);
    }

    //Retorna la resposta correcta d'una pregunta.
    public function isCorrect($id){
        $_respostes = Resposta::where('idPregunta', '=', $id)->get();

        foreach ($_respostes as $resposta){
            //Comprova si la resposta és la correcta.
            if($resposta->isCorrecta == 1){
                //Si ho es la retorna.
                return $resposta;
            }
        }
    }

    public function respostes($id){
        return Pregunta::findOrFail($id)->load('respostes');
    }
}
