<?php
/**
 * Created by PhpStorm.
 * User: cris_
 * Date: 16/03/2019
 * Time: 17:43
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ranking;
use DB;

class RankingController extends Controller
{
    public function all(){

        //Agafa totes les dades de la taula Ranking i les ordena per la columna temps.
        $ranking = DB::table('Ranking')->orderBy('temps')->get();

        return response()->json($ranking);
    }

    public function add(Request $request){
        $_ranking = new Ranking();

        $input = $request->all();
        $_ranking->fill($input)->save();

        return response()->json($_ranking);
    }
}