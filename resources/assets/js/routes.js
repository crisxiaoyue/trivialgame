import Vue from 'vue';
import Router from 'vue-router'
import 'es6-promise/auto'

Vue.use(Router);
export default new Router({
    routes:[
        {
            path: '/',
            name: 'startgame',
            component: require('./views/StartGame'),
            props:true
        },
        {
            path: '/tiradau',
            name: 'tiradau',
            component: require('./views/TiraDau'),
            props: true
        },
        {
            path:'/tiradau/idCategoria/:categoria',
            name: 'responpregunta',
            component:require('./views/ResponPregunta'),
            props:true
        },
        {
            path: '/dades',
            name: 'modalnom',
            component: require('./views/ModalNom'),
            props:true
        },
        {
            path: '/guanyador',
            name: 'guanyador',
            component: require('./views/Guanyador'),
            props:true
        },
        {
            path: '/perdedor',
            name: 'perdedor',
            component: require('./views/Perdedor'),
            props:true
        },
        {
            path: '/ranking',
            name: 'ranking',
            component: require('./views/Ranking')
        }

    ],
    mode: 'history' // per eliminar el #
})