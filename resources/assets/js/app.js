
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import VueSocketIO from 'vue-socket.io'
import router from './routes'
import Vuex from 'vuex'
import Toasted from 'vue-toasted';

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// inicio l'app que connecta la resta de vistes .vue
Vue.component('app', require('./components/AppComponent.vue'));
Vue.component('startGame', require('./views/StartGame.vue'));

Vue.use(Toasted)
Vue.use(Vuex);


Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:3000' //options object is Optional
}));

const app = new Vue({
    el: '#app',
    router
});
