<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/layouts/index');
});


Route::get('/juga',function () {
    return view('dau');
});

Route::get('/categoria{id}',function () {
    return view('categoria');
});

Route::get('/pregunta{id}',function () {
    return view('pregunta');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
