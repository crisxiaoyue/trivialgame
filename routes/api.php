<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function (){
    // '/api/v1/'
    Route::prefix('categories')->group(function(){
        // '/api/v1/categories/'
        Route::get('/', 'CategoriaController@all');
        Route::get('/{id}', 'CategoriaController@id');
        Route::put('/{id}', 'CategoriaController@update');//Editar
        Route::post('/', 'CategoriaController@add');//Afegir
        Route::delete('/{id}', 'CategoriaController@delete');
    });

    Route::prefix('preguntes')->group(function(){
        // '/api/v1/categories/'
        Route::get('/', 'PreguntaController@all');
        Route::get('/{id}', 'PreguntaController@id');
        Route::get('/{id}/respostes', 'PreguntaController@respostes');
        Route::get('/categoria/{categoria}', 'PreguntaController@categoria');
        Route::put('/{id}', 'PreguntaController@update');//Editar
        Route::post('/', 'PreguntaController@add');//Afegir
        Route::delete('/{id}', 'PreguntaController@delete');
        Route::get('/correcta/{id}', 'PreguntaController@isCorrect');
    });

    Route::prefix('respostes')->group(function(){
        // '/api/v1/categories/'
        Route::get('/', 'RespostaController@all');
        Route::get('/{id}', 'RespostaController@id');
        Route::put('/{id}', 'RespostaController@update');//Editar
        Route::post('/', 'RespostaController@add');//Afegir
        Route::delete('/{id}', 'RespostaController@delete');
        Route::get('/correcta/{id}', 'RespostaController@isCorrect');
    });

    Route::prefix('ranking')->group(function(){
        // 'api/v1/ranking/'
        Route::get('/','RankingController@all');
        Route::post('/','RankingController@add');
    });
});

